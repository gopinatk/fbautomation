# Getting Started with Gauge
This is an executable specification file. This file follows markdown syntax. Every heading in this file denotes a scenario. Every bulleted point denotes a step.
To execute this specification, use
	npm test

## Login NWLS
* Goto login page
* Provide inputs username & password
* Page contains "We are experiencing rate fluctuations from the Shipping Lines"

## Search for Rates
* Provide Origin & Destination port
* Rate Search for Door to Port
* Filter liners
* View costBreak up of the rate card and assert the legs charges

## Proceed to bookings
* Proceed to Quote
* Generate the quote
* Book the quote