const {openBrowser, write,switchTo, closeBrowser, goto, moveBy, press, text, focus, textBox, waitFor, toRightOf,click,
    reload,title,into,$,dropDown,mouseAction,rightClick,evaluate,clear,scrollDown} = require('taiko');

    const loginPage  = require('./login.js');

class InstantRates {
    constructor() {
        const fclOriginPort = $("//*[@name='fcl_origin_port']");
        const originPort = 'NERUL';
        const roadOrigin = $("//div[text()='NERUL']");
        const fclDestinationPort = $("//*[@name='fcl_destination_port']");
        const destinationPort = 'AUSYD';
        const destPort = $("//span[text()='AUSYD']");
        const text = 'ADDITIONAL SERVICES';
        const destCharge = '';
        const originCust = '';
        const destCust = '';
        const search = 'SEARCH';
        const liner = $("//text()[.='Filter By Liners']/ancestor::md-select[1]");
        const linerMaeu = 'MAERSK';
        const shareButton = $("span.pointer.pv-5.pr-10 > md-icon");
        const doneButton = $("//text()[.='DONE']/ancestor::button[1]");
        const proceedButton = 'PROCEED';
        const nextButton = $("//text()[.='NEXT']/ancestor::button[1]");
        const Closedialog = $("md-dialog > button > md-icon");
        const emailAddress = $("//*[@name='EmailAddress']");
        const emaill = "test@adfada.com";
        const generateQuote = $("//text()[.='GENERATE QUOTATION']/ancestor::button[1]");
        const viewQuote = $("//text()[.='VIEW QUOTE']/ancestor::button[1]");
       
        this.enterOriginPort = async function(){
            waitFor(fclOriginPort);
            await click(fclOriginPort);
            await write(originPort,into(fclOriginPort));
            await click(roadOrigin);
        }

        this.enterDestinationPort = async function(){
            await click(fclDestinationPort);
            await write(destinationPort,into(fclDestinationPort));
            await click(destPort);
        }

        this.clickSearch = async function(){
            await click(search);
            await waitFor(15000);
        }

        this.clickLiners = async function(){
            await click(liner);
            await click(linerMaeu);
            await mouseAction('press', {x:1000,y:500});
            await mouseAction('move', {x:1000,y:500});
            await mouseAction('release', {x:1000,y:500});
        }
        
        this.viewCost = async function(){
            await click("View cost Breakup");
            await waitFor(2000);
            await click(Closedialog);
        }

        this.clickAddtoQuote = async function(){
            await waitFor(2000);
            await click(shareButton);
            await click(doneButton);
            await waitFor(2000);
            await click(proceedButton);
            await click(nextButton);
        }
    }
}
module.exports=new InstantRates();