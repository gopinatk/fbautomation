const {openBrowser, write,switchTo, closeBrowser, goto, moveBy, press, text, focus, textBox, waitFor, toRightOf,click,
    reload,title,into,$,dropDown,mouseAction,rightClick,evaluate,clear,scrollDown} = require('taiko');

class quoteGen {
    constructor() {
        const emailAddress = $("//*[@name='EmailAddress']");
        const emaill = "test@adfada.com";
        const generateQuote = $("//text()[.='GENERATE QUOTATION']/ancestor::button[1]");
        const viewQuote = $("//span[contains(text(),'QBB000')]");
        const bookNow = $("//text()[contains(.,'BOOK NOW')]/ancestor::button[1]");
        const searchCommodity = $("//*[@name='commodityName']");
        const commodityName = 'Rice Farming';
        const riceCommodity = $("//span[text()='Rice Farming']");
        const requestBooking = $("//text()[.='REQUEST BOOKING']/ancestor::button[1]");
        const viewShipment = $("//u[contains(text(),'View Shipment here')]");

        this.generateQuote = async function(){
            await write(emaill,into(emailAddress));
            await waitFor(generateQuote);
            await click(generateQuote);
            await waitFor(2000);
            await click(viewQuote);
        }

        this.BookQuote = async function(){
            await click(bookNow);
            await waitFor(2000);
            await scrollDown('COMMODITY');
            await click(searchCommodity);
            await write(commodityName,into(searchCommodity));
            await click(riceCommodity);
            await click(requestBooking);
            await waitFor(4000);
            await click(viewShipment);
        }
    }
}
module.exports=new quoteGen();