const {openBrowser, write,switchTo, closeBrowser, goto, press, text, focus, textBox, waitFor, toRightOf,click,
    reload,title,into,$,clear,scrollDown} = require('taiko');

class loginPage {
    constructor() {
        const userName = textBox({id:'userName'});
        const user = 'fwdadmn01@gmail.com';
        const passWord = $("//input[@type='password'][@name='password']");
        const pass = 'ERTY6783$*';
        const login = 'LOGIN';

        this.enterUserName = async function(){
            await waitFor(userName);
            await write(user,into(userName));
        }

        this.enterPassword = async function(){
            //await waitFor(passWord);
            await click(passWord);
            await write(pass,into(passWord));
        }

        this.clickLogin = async function(){
            await click(login);

        }
    }
}
module.exports=new loginPage();