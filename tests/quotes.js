/* globals gauge*/
"use strict";
const { openBrowser,write, closeBrowser, goto, press, screenshot, text,$,focus, click, textBox, toRightOf } = require('taiko');
const assert = require("assert");

step("Proceed to Quote",async function(){
    var pr = require('../pages/searchrates.js');
    await pr.clickAddtoQuote();
});

step("Generate the quote", async function() {
    var qu = require('../pages/quotegenerate.js');
    await qu.generateQuote();
});

step("Book the quote", async function() {
    var qu = require('../pages/quotegenerate.js');
    await qu.BookQuote();
});
