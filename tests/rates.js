/* globals gauge*/
"use strict";
const { openBrowser,write, closeBrowser, goto, press, screenshot, text,$,focus, click, textBox, toRightOf } = require('taiko');
const assert = require("assert");
var login = require('../pages/login.js');


step("Provide Origin & Destination port", async function() {
    var sr = require('../pages/searchrates.js');

    await sr.enterOriginPort();
    await sr.enterDestinationPort();
});

step("Rate Search for Door to Port", async function() {
    var sr = require('../pages/searchrates.js');
    await sr.clickSearch();
});

step("Filter liners", async function() {
    var sr = require('../pages/searchrates.js');
    await sr.clickLiners();
});

step("View costBreak up of the rate card and assert the legs charges",async function(){
    var sr = require('../pages/searchrates.js');
    await sr.viewCost();
});

step("Assert |CHARGES|UNIT PRICE|QTY|AMOUNT| <arg0>", async function(CHARGES,QTY,AMOUNT) {
    assert.ok(await text(CHARGES).exists());
    var sr = require('../pages/searchrates.js');
    await sr.closeModal();
    });