/* globals gauge*/
"use strict";
const { openBrowser,write, closeBrowser, goto, press, screenshot, text,$,focus, click, textBox, toRightOf } = require('taiko');
const assert = require("assert");
const headless = process.env.headless_chrome.toLowerCase() === 'true';

beforeSuite(async () => {
    await openBrowser({ headless: headless },{args: ["--start-fullscreen"]})
});

afterSuite(async () => {
    await closeBrowser()
});

gauge.customScreenshotFn = async function() {
    return await screenshot({ encoding: 'base64' });
};