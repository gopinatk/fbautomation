/* globals gauge*/
"use strict";
const { openBrowser,write, closeBrowser, goto, press, screenshot, text,$,focus, click, textBox, toRightOf } = require('taiko');
const assert = require("assert");

step("Goto login page", async () => {
    await goto('http://fwdb.qaship.freightbro.com/login');
});

step("Provide inputs username & password", async function() {
    var fp = require('../pages/login.js');
    await fp.enterUserName();
    await fp.enterPassword();
    await fp.clickLogin();
}); 

step("Page contains <content>", async (content) => {
    assert.ok(await text(content).exists());
});
